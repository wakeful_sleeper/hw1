package com.company.task2_Point;

public class Circle {
    private Point center;
    private double radius;

    public Circle(Point center, double radius) {
        this.center = center;
        this.radius = radius;
    }

    /**
     * containsPoint method is checking whether the point inside of the circle;
     *
     * @param p specified point;
     * @return true/false;
     */
    public boolean containsPoint(Point p) {
        return p.distanceTo(center) <= radius;
    }
}
