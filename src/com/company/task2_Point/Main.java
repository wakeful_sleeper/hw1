package com.company.task2_Point;

import java.util.Scanner;

import static com.company.task2_Point.PointList.points;

public class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        PointList pointList = new PointList(sc);
        pointList.readPoint();
        pointList.morePoints();
        Circle circle = pointList.readCircle();

        int counter = 0;

        for (int i = 0; i < points.size(); i++) {
            Point p = pointList.readPoint();
            if(circle.containsPoint(p)){
                counter++;
            }

        }
        System.out.printf("Amount of points in the circle - %d\n", counter);
    }
}
