package com.company.task2_Point;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class PointList {
    private Scanner sc;
    static List<Point> points = new ArrayList<>();

    public PointList(Scanner sc) {
        this.sc = sc;
    }

    /**
     * readPoint method is getting inserted coordinates of the point by user;
     *
     * @return point;
     */
    public Point readPoint() {
        System.out.println("Please enter X coordinates: ");
        double x = sc.nextDouble();
        System.out.println("Please enter Y coordinates: ");
        double y = sc.nextDouble();
        return new Point(x, y);
    }

    /**
     * morePoints method is checking *using while loop) whether user wants to add more points and
     * correct input;
     *
     * @return point;
     */
    public Point morePoints() {
        System.out.println("Add more points? 1 - YES; 2 - NO:");
        int choice = sc.nextInt();
        while (choice == 1) {
            readPoint();
            morePoints();
            break;
        }
        if (choice < 1 || choice > 2) {
            System.out.println("Invalid input, please try again!");
            morePoints();
        }
        return null;
    }

    /**
     * readCircle method is getting inserted coordinates of the circle by user;
     * @return circle (center and radius);
     */
    public Circle readCircle() {
        System.out.println("Enter radius: ");
        double radius = sc.nextDouble();
        System.out.println("Enter center" );
        Point center = readPoint();
        return new Circle(center, radius);
    }



}
