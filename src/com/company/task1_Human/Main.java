package com.company.task1_Human;

public class Main {

    public static void main(String[] args) {
        Human human1 = new Human(
                "Ivan",
                "Pupkin",
                "Vasilevich");

        Human human2 = new Human(
                "Ivan",
                "Pupkin");

        human1.getFullName();
        human2.getFullName();

        human1.getShortName();
        human2.getShortName();
    }
}
