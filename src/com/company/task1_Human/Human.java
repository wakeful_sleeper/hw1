package com.company.task1_Human;

public class Human {
    private String name;
    private String surname;
    private String patronymic;

    public Human(String name, String surname, String patronymic) {
        this.name = name;
        this.surname = surname;
        this.patronymic = patronymic;
    }

    public Human(String name, String surname) {
        this.name = name;
        this.surname = surname;

    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public String getPatronymic() {
        return patronymic;
    }

    /**
     * getFullName method is used to get a full name;
     * if patronymic is not null - getting surname, name, patronymic;
     * if patronimic is null - getting surname and name;
     *
     * @return Nothing;
     */

    public void getFullName() {
        if (patronymic != null) {
            System.out.println(
                    this.getSurname() + " " +
                    this.getName() + " " +
                    this.getPatronymic()
            );
        } else {
            System.out.println(
                    this.getSurname() + " " +
                    this.getName()
            );
        }
    }

    /**
     * getShortName method is used to get a full surname, short name and patronymic;
     * if patronymic is not null - getting a full surname, short name, short patronymic;
     * if patronymic is null - getting a full surname and short name;
     *
     * @return Nothing;
     */

    public void getShortName() {
        if (patronymic != null) {
            System.out.println(
                    this.getSurname() + " " +
                    this.getName().toUpperCase().charAt(0) + "." +
                    this.getPatronymic().toUpperCase().charAt(0) + "."
            );
        } else {
            System.out.println(
                    this.getSurname() + " " +
                    this.getName().toUpperCase().charAt(0) + "."
            );
        }
    }
}
